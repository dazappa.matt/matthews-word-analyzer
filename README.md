# Matthew's Word Analyzer
This project is aimed at writers looking to analyze their writing.

## Features
* Show your most and least frequently used words
* Export data as .csv to analyze in spreadhseet software

## Supported File Formats
* Plain text (.txt)
If the document extension is unsupported and the file format is not specified, the program will default to plain text.

## Planned File Formats
* Open Document Text (.odt)
* Rich Text Format(.rtf)