# -*- coding: utf-8 -*- 

import sys, getopt

inputPath = ""
exportData = False
# Strips filename from inputPath, and exports there.
exportPath = ""
numUniqueWordsToDisplay = 10
# Outputs a sentence for each word for certain grammatical analyzers
showContextSentence = False

def set_input_file(file_path_and_name):
    global inputPath, exportPath
    inputPath = file_path_and_name
    # TODO
    exportPath = inputPath

try:
    opts, args = getopt.getopt(sys.argv[1:], "hi:f:")
except getopt.GetoptError:
    print("Unexpected parameter. Please run mwa.py -h for help.")
    sys.exit(2)

# TODO: add full length words to options
for opt, arg in opts:
    if opt == '-h':
        print("-h \tPrint help.")
        print("-f \tInput file to analyze.")
        print("-i \tAlias of -f")
        print("-e \tExport complete CSV data")
        sys.exit()
    elif opt in ("-f"):
        set_input_file(arg)
    elif opt in ("-i"):
        set_input_file(arg)
    elif opt == "-e":
        exportData = true

# Read file
if inputPath == "":
    print("No input file specified. Please run \"mwa -h\" for help.")
    sys.exit(2)

# TODO: catch error more nicely?
print("Reading input file "+inputPath)
f = open(inputPath, "r")
content = f.read()
f.close()

# Strip common formatting to reduce unnecessary duplicate words
print("Stripping formatting")
content = content.replace("(", "")
content = content.replace(")", "")
content = content.replace("'", "")
content = content.replace("\"", "")
content = content.replace(",", "")
content = content.replace(".", "")
content = content.replace("?", "")
content = content.replace("!", "")
content = content.replace("-", "")
content = content.replace("#", "")
content = content.replace("<", "")
content = content.replace(">", "")
content = content.replace("…", "") # Ellipsis
# TODO: em and en dashes
content = content.lower()

print("Analyzing")
# Split to array of words
words = content.split()
# [Word, Count of how many times word has been used]
uniqueWords = []
numChars = 0

for w in words:
    numChars = numChars + len(w)
    isWordUnique = True
    for uw in uniqueWords:
        if uw[0] == w:
            uw[1] = uw[1] + 1
            isWordUnique = False
            break
    if isWordUnique:
        uniqueWords.append([w, 1])

# Sort words from least to most used
leastToMost = sorted(uniqueWords, key=lambda x:x[1])
mostToLeast = sorted(uniqueWords, key=lambda x:x[1], reverse=True)

print("")
print("Total words: " + str(len(words)))
print("Unique words: " + str(len(uniqueWords)))
print("Average word length: " + str(numChars / len(words)))

outputStr = ""
numWordsDisplayed = 0
for w in mostToLeast:
    outputStr += w[0] + " (" + str(w[1]) + "), "
    numWordsDisplayed += 1
    if numUniqueWordsToDisplay > 0:
        if numWordsDisplayed > numUniqueWordsToDisplay:
            outputStr = outputStr[0:len(outputStr)-2]
            break
    
print(outputStr)
